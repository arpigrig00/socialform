
import { Route, Routes} from 'react-router-dom';
import Register from "./pages/auth/Register";
import Login from "./pages/auth/Login";
import Landing from "./pages/Landing";
import Dashboard from "./pages/dashboard/Dashboard";

function App() {
  return (
    <div className="App">
      <Routes>
             <>
              <Route path='/' element={<Landing/>}/>
              <Route path='/signup' element={<Register/>}/>
              <Route path='/signin' element={<Login/>}/>
              <Route path='/dashboard' element={<Dashboard/>}/>
              {/*<Route path='/user-management/companies' element={<CompanyContext/>}/>*/}
              {/*<Route path='/user-management/contracts' element={<ContractsContext/>}/>*/}
              {/*<Route path='/user-management/users' element={<UserList/>}/>*/}
              {/*<Route path='/user-management/add-user' element={<AddUser/>}/>*/}
              {/*<Route path='/user-management/company/:id' element={<Company/>}/>*/}
              {/*<Route path='/user-management/company' element={<CompanyAdminPage/>}/>*/}
            </>
        }
      </Routes>
    </div>
  );
}

export default App;
