import c from "./Header.module.css";
import {useLocation, useNavigate} from "react-router-dom";

const Header = ({fromAccount}) => {
    const navigate = useNavigate();
    const location = useLocation();
    return (

    <div className={c.header}>
        <div className={c.logo}>
            <img alt={'logo'} src={"/images/logo.png"} onClick={() => navigate("/")}/>
        </div>

        {
            fromAccount &&
            <div className={c.bar}>
                <p>Destinations</p>
                <p>Community</p>
                <p>Events</p>
            </div>
        }
        <div className={c.sections}>
            {!fromAccount && <button>About</button>}
            { fromAccount && <div className={c.sectionItem}><img onClick={() => console.log('logout')} alt={"logout"} src={"/images/profile-24.svg"}/><span>Username</span></div>}

            {
                fromAccount ? <div className={c.sectionItem}><img onClick={() => console.log('logout')} alt={"logout"} src={"/images/logout.svg"}/><span>Logout</span></div>
                    :
                    <button onClick={() => navigate(location.pathname === "/signup" || location.pathname === '/'      ? "/signin" :  "/signup")}>{location.pathname === "/signup" || location.pathname === '/'? "Sign in" : "Sign up"}</button>
            }

        </div>
    </div>
    )
}

export default Header;
