import c from "./styles/Login.module.css"
import c1 from "./styles/Register.module.css"
import Header from "../../components/header/Header";
import {useState} from "react";
import {useNavigate} from "react-router-dom";
import login from "./Login";

const Login = () => {
    const navigate = useNavigate();
    const [loginData, setLoginData] = useState({
        email: '',
        password: ''
    })

    const [showHidePass, setShowHidePass] = useState(true);

    const [loginError, setLoginError] = useState({
        emailError: '',
        passwordError: '',
    })
    const changeHandler = (e) => {
        setLoginData({...loginData, [e.target.name]: e.target.value});
    }

    const validateLoginData = () => {
        const emailPattern = /^[a-zA-Z\d._-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,4}$/;
        const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&.+{\]\-,;`<>':"=^#_|\\])(?=.*?[A-Za-z\d@$!%*+?&\][\-,;`<>':"=^#_|\\]).{12,}/;
        if (!emailPattern.test(loginData.email)) {
            setLoginError({...loginError, emailError: "Please enter a valid email address."})
            return false;
        }

        if (!passwordPattern.test(loginData.password)) {
            setLoginError({passwordError: "Password must be of minimum 12 characters length and contain uppercase, lowercase, special characters and digits.", emailError: '', passwordMismatchError: ''})
            return false;
        }
        setLoginError({...loginError, passwordError: '',})
        return true;
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const isValid = validateLoginData();
        console.log("is valid", isValid)

        if (isValid) {
            navigate('/dashboard');
        }
    }
    return (
        <div className={c.login}>
            <Header />
            <div className={c.loginWrapper}>
                <div className={c.loginWall}>
                    <img alt={'login'} src={"/images/login.png"} width={650}/>
                </div>
               <div className={c.loginContent}>
                   <div className={c.loginIntro}>
                       <h1 >Sign in to SoloTravel</h1>
                   </div>
                   <div className={c.loginForm}>
                       <div className={c1.inputWrapper}>
                           <label><p>E-mail</p></label>
                           <div className={c1.registrationContent}>
                               <input autoComplete={'current-password'} type='text' onChange={changeHandler} name='email' value={loginData.email} required/>
                               <small className={c1.warningText}>{loginError.emailError}</small>
                           </div>
                       </div>

                       <div className={c1.inputWrapper}>
                           <label><p>Password</p></label>
                           <div className={c1.passwordContent}>
                               <input autoComplete={'current-password'} type='password' placeholder='************' onChange={changeHandler} name='password' value={loginData.password} required/>
                               {showHidePass ? <img alt='hide' src={'/images/eye-blind-icon.svg'} onClick={() => setShowHidePass(!showHidePass)}/> : <img alt='show' src={'/images/eye-icon.svg'} onClick={() => setShowHidePass(!showHidePass)}/>}
                           </div>
                           <small className={c1.warningText}>{loginError.passwordError}</small>

                       </div>
                       <div className={c.loginAddActions}>
                           <p className={c.forgotPassword} onClick={() => console.log("forgot")}>Forgot password  </p>

                           <p className={c.toRegisterP}>Don't have account yet ? <span className={c.toRegister} onClick={() => navigate("/signup")}>Register</span></p>
                       </div>

                       <div className={`${c1.submit} ${c.loginAction}`}>
                           <button type='submit' onClick={handleSubmit}>Sign In</button>
                       </div>
                   </div>

               </div>
            </div>
        </div>
    )
}

export default Login;
