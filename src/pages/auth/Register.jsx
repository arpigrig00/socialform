import c from './styles/Register.module.css'
import {useState} from "react";
import Header from "../../components/header/Header";

const Register = () => {
    const [registrationData, setRegistrationData] = useState({
        username: '',
        firstName: '',
        lastName: '',
        email: '',
        dob: '',
        password: '',
        repeatPassword: ''
    });

    const [registrationError, setRegistrationError] = useState({
        emailError: '',
        passwordError: '',
        passwordMismatchError: ''
    })

    const [showHidePass, setShowHidePass] = useState(true);

    const changeHandler = (e) => {
        setRegistrationData({...registrationData, [e.target.name]: e.target.value});
    }

    const validateRegistrationData = () => {
        const emailPattern = /^[a-zA-Z\d._-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,4}$/;
        const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&.+{\]\-,;`<>':"=^#_|\\])(?=.*?[A-Za-z\d@$!%*+?&\][\-,;`<>':"=^#_|\\]).{12,}/;
        if (!emailPattern.test(registrationData.email)) {
            setRegistrationError({...registrationError, emailError: "Please enter a valid email address."})
            return false;
        }

        if (registrationData.password !== registrationData.repeatPassword) {
            setRegistrationError({...registrationError, passwordMismatchError: "Passwords do not match. Please re-enter your password.", emailError: ''})
            return false;
        }

        if (!passwordPattern.test(registrationData.password)) {
            setRegistrationError({passwordError: "Password must be of minimum 12 characters length and contain uppercase, lowercase, special characters and digits.", emailError: '', passwordMismatchError: ''})
            return false;
        }
        setRegistrationError({...registrationError, passwordError: '',})
        return true;
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const isValid = validateRegistrationData();
        if (isValid) {
            console.log("do request for registration")
        }
    }

    return (
        <div className="App">
            <Header />
            <div className={c.registrationWrapper}>
                <div className={c.registrationWall}>
                    <img alt={'wallpaper'} src={'/images/register.webp'} width={550}/>
                </div>
                <div className={c.registrationContainer}>
                    <div className={c.registrationIntro}>
                        <h1 >Sign up to SoloTravel</h1>
                        <p>Share Your Travel Story: Join the Social Revolution</p>
                    </div>
                    <div className={c.registrationForm}>
                        <form onSubmit={handleSubmit}>
                            <div className={c.inputWrapper}>
                                <label><p>Firstname</p></label>
                                <div className={c.registrationContent}>
                                    <input autoComplete={'current-password'} type='text' onChange={changeHandler} name='firstName' value={registrationData.firstName} required/>
                                </div>
                            </div>

                            <div className={c.inputWrapper}>
                                <label><p>Lastname</p></label>
                                <div className={c.registrationContent}>
                                    <input autoComplete={'current-password'} type='text' onChange={changeHandler} name='lastName' value={registrationData.lastName} required/>
                                </div>
                            </div>

                            <div className={c.inputWrapper}>
                                <label><p>E-mail</p></label>
                                <div className={c.registrationContent}>
                                    <input autoComplete={'current-password'} type='text' onChange={changeHandler} name='email' value={registrationData.email} required/>
                                    <small className={c.warningText}>{registrationError.emailError}</small>
                                </div>
                            </div>
                            <div className={c.inputWrapper}>
                                <label><p>Date of Birth</p></label>
                                <div className={c.registrationContent}>
                                    <input autoComplete={'current-password'} type='date' onChange={changeHandler} name='dob' value={registrationData.dob} required/>
                                </div>
                            </div>

                            <div className={c.inputWrapper}>
                                <label><p>Password</p></label>
                                <div className={c.passwordContent}>
                                    <input autoComplete={'current-password'} type='password' placeholder='************' onChange={changeHandler} name='password' value={registrationData.password} required/>
                                    {showHidePass ? <img alt='hide' src={'/images/eye-blind-icon.svg'} onClick={() => setShowHidePass(!showHidePass)}/> : <img alt='show' src={'/images/eye-icon.svg'} onClick={() => setShowHidePass(!showHidePass)}/>}
                                </div>
                            </div>
                            <div className={c.inputWrapper}>
                                <label><p>Confirm password</p></label>
                                <div className={c.passwordContent}>
                                    <input autoComplete={'current-password'} type={showHidePass ? 'password' : 'text'} placeholder='************' onChange={changeHandler} name='repeatPassword' value={registrationData.repeatPassword} required/>

                                    {showHidePass ? <img alt='hide' src={'/images/eye-blind-icon.svg'} onClick={() => setShowHidePass(!showHidePass)}/> : <img alt='show' src={'/images/eye-icon.svg'} onClick={() => setShowHidePass(!showHidePass)}/>}
                                </div>
                                <small className={c.warningText}>{registrationError.passwordMismatchError}</small>
                                <small className={c.warningText}>{registrationError.passwordError}</small>
                            </div>
                            <div className={c.submit}>
                                <button type='submit'>Sign up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;
