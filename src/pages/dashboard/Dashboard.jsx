import c from "./styles/Dashboard.module.css"
import Header from "../../components/header/Header";
import DestinationItem from "./components/DestinationItem";
import EventItem from "./components/EventItem";
import CommunityItem from "./components/CommunityItem";

const Dashboard = () => {
    return (
        <div className={c.dashboard}>
            <Header fromAccount={true} />
            <div className={c.dashboardContainer}>
                <div className={c.dashboardBody}>
                    <div className={c.destinationSection}>
                        <div className={c.destinationTitle}>
                            <img alt={"destination"} src={"/images/destination-3.svg"} />
                            <p className={c.title}>Famous destinations</p>
                        </div>

                        <div className={c.destinationBody}>
                            <div className={c.destinationInner}>
                                <DestinationItem />
                                <DestinationItem />
                                <DestinationItem />
                            </div>
                        </div>

                        <div className={c.sectionFooter}>
                            <p> View more </p><img alt={'view'} src={"/images/arrow-52.svg"}  />
                        </div>
                    </div>

                    <div className={c.eventSection}>
                        <div className={c.eventTitle}>
                            <img alt={"event"} src={"/images/event-12.svg"} />
                            <p className={c.title}>Events</p>
                        </div>
                        <div className={c.eventBody}>
                            <div className={c.eventInner}>
                                <EventItem classname={`${c.eventImg1}`} type={"Celebration"}/>
                                <EventItem  classname={`${c.eventImg}`} type={"Concert"}/>
                            </div>
                        </div>

                        <div className={c.sectionFooter}>
                            <p> View more </p><img alt={'view'} src={"/images/arrow-52.svg"}  />

                        </div>
                    </div>

                    <div className={c.communitySection}>
                        <div className={c.communityTitle}>
                            <img alt={"community"} src={"/images/community-14.svg"} />
                            <p className={c.title}>Community</p>
                        </div>
                        <div className={c.communityBody}>
                            <div className={c.communityInner}>
                                <CommunityItem/>
                                <CommunityItem />
                                <CommunityItem />
                            </div>
                        </div>

                        <div className={c.sectionFooter}>
                           <p> View more </p><img alt={'view'} src={"/images/arrow-52.svg"}  />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dashboard;
