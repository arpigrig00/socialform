import c from "../styles/Dashboard.module.css"
import {Rating} from 'react-simple-star-rating'

const DestinationItem = () => {
    const handleRating = (e) => {
        console.log(e)
    }
    return (
        <div className={c.item}>
            <div className={c.itemContent}>
                <div className={c.itemTitle}>
                    <div>
                        <p>Name</p>
                        <p>Country , City</p>
                    </div>
                    <div className={c.itemRating}>
                        <Rating
                            onClick={handleRating}
                            initialValue={2}
                            size={20}
                            label
                            transition
                            fillColor='orange'
                            emptyColor='gray'
                        />
                    </div>

                </div>
                <div className={c.itemBody}>
                    <img alt={"destImg"} src={"/images/maxresdefault.jpg"} width={200}/>
                    <p className={c.itemDescription}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>


            </div>

        </div>
    )
}

export default DestinationItem;
