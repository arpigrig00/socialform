import c from "../styles/Dashboard.module.css"

const EventItem = ({classname, type}) => {
    return (
        <div className={c.eventItem}>
            <div className={c.eventItemContent}>
                <div className={classname}>
                </div>
                <div className={c.eventType}>
                    {type}
                </div>
                <div className={c.eventDesc}>
                    <p>Place</p>
                    <p>Date Time</p>
                </div>
            </div>
        </div>
    )
}

export default EventItem;
