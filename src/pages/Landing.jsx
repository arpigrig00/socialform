import {useNavigate} from "react-router-dom";
import Header from "../components/header/Header";

import c from "./Landing.module.css"

const Landing = () => {
    const navigate  = useNavigate();
    return (
            <div className={c.landing}>
                <Header />
                <div className={c.landingContainer}>
                    <div className={c.landingWrapper}>
                        <div className={c.landingDescWrapper}>
                            <p className={c.landingTitle}>Welcome to SoloTravel</p>
                            <p className={c.landingSubTitle}>Your Journey Awaits</p>
                            <p className={c.description}>
                                <i>Discover the thrill of solo travel like never before! SoloWander is your ultimate companion for adventurous, independent exploration.
                                    Whether you're a seasoned solo traveler or embarking on your very first solo adventure, we've got you covered.</i>
                            </p>

                            <div className={c.landingDescButton}>
                                <button  onClick={() => navigate("/signup")}>Get Started</button>
                            </div>
                        </div>
                        <div className={c.landingImgWrapper}>
                            <img alt={'landing' } src={'/images/landing.png'} width={800}/>
                        </div>
                    </div>
                </div>

            </div>
        )

}

export default Landing;
